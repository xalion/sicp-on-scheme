;; Обёртка для вычисления корня
(define (sqrt x)
  ;; Вычисление корня через метод Ньютона
  (define (sqrt-iter guess)
    (if (good-enough guess) guess
        (sqrt-iter (improve guess))
    )
  )

  ;; Вычисление нового числа-догадки
  (define (improve guess)
    (average guess (/ x guess))
  )

  ;; Вычисление среднего арифметического
  (define (average guess quotient-x-on-guess)
    (/ (+ guess quotient-x-on-guess) 2)
  )

  ;; Проверка на точность ответа
  (define (good-enough guess)
    (< (abs (- (square guess) x)) 0.01)
  )

  ;; Вычисление модуля числа
  (define (abs num)
    (if (< num 0) (- num) num)
  )

  ;; Вычисление квадрата числа
  (define (square num)
    (* num num)
  )

(sqrt-iter 1.0)
)